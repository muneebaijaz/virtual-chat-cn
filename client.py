#!/usr/bin/env python3
"""Script for Tkinter GUI chat client."""
from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import time
import tkinter
import os

def receive():
    """Handles receiving of messages."""
    while True:
        try:
            msg = client_socket.recv(BUFSIZ).decode("utf8")
            msg_list.insert(tkinter.END, msg)


            if msg=="filebroadcast":
                nam = client_socket.recv(1024)
                print(nam)
                filesize = client_socket.recv(1024)
                print(filesize)
                ss=str(nam.decode('utf-8'))
                fsize = int(filesize.decode('utf-8'))
                ss=ss+"_fromserver"

                try:
                    with open(nam, 'wb') as fr:
                           
                           
                           print(fsize)
                           rsize = 0
                           while True:
                                    data = client_socket.recv(1024)
                                    rsize = rsize + len(data)
                                    fr.write(data)
                                    if  rsize >= fsize:
                                        print('Breaking from file write')
                                        break
                finally:               
                        fr.close()

                print( "done Data recieving from server...." ) 

        except OSError:  # Possibly client has left the chat.
            break



 
def send(event=None):  # event is passed by binders.
    """Handles sending of messages."""
    msg = my_msg.get()
    my_msg.set("")  # Clears input field.
    client_socket.send(bytes(msg, "utf8"))
    if msg == "{quit}":
        client_socket.close()
        top.quit()

    if msg == "{sleep}":

        x=input("enter time of suspension")

        x=int(x, base=10)
        time.sleep(x)

    if msg=="{send}":

        x=input("Enter filename:")
                                
        

        file=x+".txt"
        print(file)
        fsize = os.path.getsize(file)

        client_socket.send(str(file).encode('utf-8'))
        client_socket.send(str(fsize).encode('utf-8'))

        
        print(fsize)

        


        print( "Sending Data ...." )     

        f = open(file,"rb")    
        a = f.read()

        client_socket.sendall(a)    

        f.close()

        
        
        #if you haven't send file, then recieve from server
  
    
   
        
def on_closing(event=None):
    """This function is to be called when the window is closed."""
    my_msg.set("{quit}")
    send()

top = tkinter.Tk()
top.title("Chatter")

messages_frame = tkinter.Frame(top)
my_msg = tkinter.StringVar()  # For the messages to be sent.
my_msg.set("Type your messages here.")
scrollbar = tkinter.Scrollbar(messages_frame)  # To navigate through past messages.
# Following will contain the messages.
msg_list = tkinter.Listbox(messages_frame, height=15, width=50, yscrollcommand=scrollbar.set)
scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
msg_list.pack()
messages_frame.pack()

entry_field = tkinter.Entry(top, textvariable=my_msg)
entry_field.bind("<Return>", send)
entry_field.pack()
send_button = tkinter.Button(top, text="Send", command=send)

send_button.pack()

top.protocol("WM_DELETE_WINDOW", on_closing)

#----Now comes the sockets part----
HOST = input('Enter host: ')
PORT = input('Enter port: ')
if not PORT:
    PORT = 33000
else:
    PORT = int(PORT)

BUFSIZ = 1024
ADDR = ("10.7.17.17", 5)

client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect(ADDR)

receive_thread = Thread(target=receive)
receive_thread.start()
tkinter.mainloop()  # Starts GUI execution
