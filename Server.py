from socket import AF_INET, socket, SOCK_STREAM 
import threading
import time
import os
sleep=[]

def filesend(file,client):
        
        print('Here we gonna send to all clients the new file')

        fileclient=list(clients)

        fileclient.remove(client)

        


        fsize = os.path.getsize(file) #filesize
        
        x=0

        for client_socket in fileclient:
                
                client_socket.send(bytes("filebroadcast", "utf8"))
                
                client_socket.send(str(file).encode('utf-8'))
                time.sleep(1)
                client_socket.send(str(fsize).encode('utf-8'))
                
                
                f = open(file,"r")    
                a = f.read()
                while True:      
                        for line in a:
                            client_socket.send(bytes(line,"utf8"))    
                        break
            
                f.close()
                x=x+1



def clientcode(client_socket):  # Takes client socket as argument.
                                #Handles a single client connection


        name = client_socket.recv(buffer).decode("utf8") #whatever the client has sent will be his name.

        welcome = 'Welcome %s! If you ever want to quit, type {quit} to exit.' % name


        client_socket.send(bytes(welcome, "utf8"))

        msg = "%s has joined the chat!" % name #display the name of client in chat

        broadcast(bytes(msg, "utf8"))

        clients[client_socket]=name #save name of client;socket number is the key i.e in dictionary it will be saved as {123: ammara, }

        while True:

                msg = client_socket.recv(buffer) #whatever the client has sent will be his message
        
                flag=0
                
                if msg != bytes("{quit}", "utf8"):

                        if msg==bytes("{sleep}","utf8"):
                                
                                
                                time.sleep(15)

                                
                                
                        if msg==bytes("{send}","utf8"):
                                flag=1

                                nam = client_socket.recv(1024)
                                filesize = client_socket.recv(1024)

                                ss=str(nam.decode('utf-8'))

                                print (ss)

                                newname=ss+"_new"+".txt"

                                print(msg)

                                fsize = int(filesize.decode('utf-8'))

                                try:
                                        with open(newname, 'wb') as fr:
                                               
                                               
                                               print(fsize)
                                               rsize = 0
                                               while True:
                                                        data = client_socket.recv(1024)
                                                        rsize = rsize + len(data)
                                                        fr.write(data)
                                                        if  rsize >= fsize:
                                                            print('Breaking from file write')
                                                            break
                                finally:               
                                        fr.close()

                                print("Complete recieving", "utf8")

                                filesend(newname,client_socket)

                                
                                
                                

                        
                        if msg==bytes("{name}","utf8"):
                                flag=1
                                client_socket.send(bytes("Please send new name", "utf8"))

                                new_name=client_socket.recv(buffer).decode("utf8")

                                name=new_name
                                
                                clients[client_socket]=name
                                       
                                        
                        if msg==bytes("{block}","utf8"):
                                flag=1
                                client_socket.send(bytes("Please name of user to be blocked", "utf8"))

                                new_name=client_socket.recv(buffer).decode("utf8")

                

                                for key,value in clients.copy().items():
                                       if value == new_name:
                                               

                                                block[key]=value
                                                broadcast(bytes("%s has been unblocked." % new_name, "utf8"))
                                                clients.pop(key)
                               


                        if msg==bytes("{unblock}","utf8"):
                                flag=1
                                client_socket.send(bytes("Please name of user to be unblocked", "utf8"))

                                new_name=client_socket.recv(buffer).decode("utf8")

   
                                for key,value in block.copy().items(): #select the key having value as name of user to be unblocked
                                        
                                        if value == new_name:
                                                clients[key]=value
                                                block.pop(key)
                                        
                                broadcast(bytes("%s has been unblocked." % new_name, "utf8"))

                        
                        
                        if client_socket not in block and flag==0 and msg!=bytes("{sleep}","utf8"):
                                        broadcast(msg, name+": ")


                
                else:

                    client_socket.send(bytes("{quit}", "utf8")) #send client quit message
                    client_socket.close() #causes client specific socket to close
                    del clients[client_socket]  #remove the key and the corresponding value of key
                    broadcast(bytes("%s has left the chat." % name, "utf8"))
                    break


def broadcast(msg, prefix=""):  # prefix is for name identification.
    #Broadcasts a message to all the clients

    for sock in clients: #all the socket number saved in the client list, send them the message posted by the particular client

        sock.send(bytes(prefix, "utf8")+msg) #name: message
                                


server = socket(AF_INET, SOCK_STREAM) #sock_stream is used to define tcp port, Af_inet used for ipv4 protocols to connect to server

ip=""

socket_no=5

buffer=2048

addresses={} #dictionary for storing the client.
clients = {}
block={}

server_id=(ip,socket_no)

server.bind(server_id)  #parameter the client should know, bind accepts a tuple

server.listen(100)  #listens for 100 active connections, listening socket created it will allow server to accept




while True:
        client_socket, client_address = server.accept() # client_socket is a socket object for that user
                                                        # address contains the IP address of the client 
                                                
    
        print("%s:%s has connected." % client_address)
        client_socket.send(bytes("Welcome to the chat"+"Now type your name and press enter!", "utf8")) #client_socket is socket and client_socket.send() sends data into that socket

        addresses[client_socket]=client_address #using client_socket as key
        
       
        t=threading.Thread(target=clientcode, args=(client_socket,)) #thread=functionname,args=(argument of that function)
        t.start()
        #every time a client connects, thread created for that client and the working starts



